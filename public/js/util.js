function padzero(num) {
    return ("0" + num).slice(-2);
}

function showdate() {
var week = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];
    let today = new Date();
    document.getElementById('date').innerText = today.getFullYear() + '-' + padzero(today.getMonth()+1) + '-' + padzero(today.getDate()) + ' ' + week[today.getDay()];
}

function startTime() {
    let today = new Date();
    let h = today.getHours();
    let m = today.getMinutes();
    let s = today.getSeconds();
    m = padzero(m);
    s = padzero(s);
    document.getElementById('clock').innerText =
        h + ":" + m + ":" + s;
    let t = setTimeout(startTime, 1000);
}

function checkTime(i) {
    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}

document.addEventListener("DOMContentLoaded", function () {
    showdate();
    startTime();
})
